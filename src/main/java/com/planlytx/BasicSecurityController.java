package com.planlytx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicSecurityController {

	private static final Logger logger = LogManager.getLogger(BasicSecurityController.class);

	@GetMapping("/getMsg")
	public String getMsg() {
		logger.info("Request came here ");
		return "welcome to springSecurity";
	}
}
